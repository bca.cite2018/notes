#include <iostream>
#include<fstream>
#include<string>
using namespace std;

class FileHandler
{
    fstream myfile;
    string filename;
    int linecount;

public:
    FileHandler(string passed_filename)
    {
        this -> filename = passed_filename;
        this -> linecount = 0;
    }

    int read()
    {
        this->myfile.open(this -> filename, ios::in);
        if(!myfile.is_open())
        {
            cout<<"Read failed"<<endl;
            return -1;
        }
        else
        {
            string line;
            while(getline(myfile, line))
            {
                cout <<"Content from file: "<<line<<endl;
            }
            myfile.close();
            return 1;
        }   
    }
    
    int write(string text_to_write)
    {
        myfile.open(this -> filename, ios::out);
        if(!myfile.is_open())
        {
            cout<<"Write failed"<<endl;
            return -1;
        }
        else
        {
            linecount = 1;
            myfile<<this->linecount<<": "<<text_to_write<<endl;
            myfile.close();
            cout<<"Write successful"<<endl;
            return 1;
        }
    }
    
    int append(string text_to_append)
    {
        myfile.open(this-> filename, ios::app);
        if(!myfile.is_open())
        {
            cout<<"Append failed"<<endl;
            return -1;
        }
        else
        {
            linecount += 1;
            myfile<<this->linecount<<": "<<text_to_append<<endl;
            myfile.close();
            cout<<"Append successful"<<endl;
            return 1;
        }
    }
    
};

int main()
{
    FileHandler file = FileHandler("Hello.txt");
    file.write("Hiii");
    file.append("Second line");
    file.read();
    return 0;
}