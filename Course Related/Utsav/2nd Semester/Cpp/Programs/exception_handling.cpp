#include <iostream>
using namespace std;

int main()
{
    float height, weight, bmi;
    cout<<"Enter your height in meter: ";
    cin>>height;
    cout<<"Enter your weight in kg: ";
    cin>>weight;
    try
    {
        if(height > 3)
        {
            float throwing_num = 1.234;
            throw throwing_num; // We throw float type to later catch
            return -1;
        }
        if(weight > 150)
        {
            int throwing_num = 1234;
            throw throwing_num; // We throw int type to later catch
            return -1;
        }
    }
    catch (float)
    {
        // Catches the float type
        cout<<"Height is too much"<<endl;
        return -1;
    }
    catch (int)
    {
        // Catches the int type
        cout<<"Weight is too much"<<endl;
        return -1;
    }
    catch(...)
    {
        // Catch any error
        cout<<"Unknown error occured"<<endl;
        return -1;
    }
    bmi = weight / (height * height); // BMI is weight / height squared
    cout<<"Your Body Mass Index is: "<<bmi;
    return 0;
}