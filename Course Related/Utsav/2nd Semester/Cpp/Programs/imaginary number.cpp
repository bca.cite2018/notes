#include <iostream>
using namespace std;

template <class T>
class ComplexNum
{
    T real_num, inum;
public:
    ComplexNum(T passed_num, T passed_i_num)
    {
        this -> real_num = passed_num;
        this -> inum = passed_i_num;
    }
    
    ComplexNum()
    {
        this -> real_num = 0;
        this -> inum = 0;
    }
    
    ComplexNum operator+(ComplexNum const& another_complexnumber)
    {
        ComplexNum resulting_num;
        resulting_num.real_num = this -> real_num + another_complexnumber.real_num;
        resulting_num.inum = this -> inum + another_complexnumber.inum;
        return resulting_num;
    }
    
    void print()
    {
        cout<<"Real: "<<this->real_num<<" Imaginary: "<<this->inum<<endl;
    }
    
};

int main()
{
    ComplexNum<int> Num1 = ComplexNum(2, 3);
    ComplexNum<int> Num2 = ComplexNum(1, 1);
    ComplexNum Num3 = Num1 + Num2;
    Num3.print();
    return 0;
}
    