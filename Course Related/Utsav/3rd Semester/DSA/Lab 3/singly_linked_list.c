#include<stdlib.h>
#include <stdio.h>
void create();
void display();
void insert_begin();
void insert_end();
void insert_pos();
void delete_begin();
void delete_end();
void delete_pos();
struct node
{
int info;
struct node *next;
};
struct node *start=NULL;
int main()
{
int choice;
printf("\n\t MENU\n");
printf("\n\t 1.Create\n");
printf("\n\t 2.Display\n");
printf("\n\t 3.Insert at the beginning\n");printf("\n\t 4.Insert at the end\n");
printf("\n\t 5.Insert at specified position\n");
printf("\n\t 6.Delete from beginning \n");
printf("\n\t 7.Delete from the end\n");
printf("\n\t 8.Delete from specified position\n");
printf("\n\t 9.Exit Program\n");
while(1){
printf("\n\t Enter your choice:");
scanf("\n\t %d",&choice);
switch(choice)
{
case 1:
create();
break;
case 2:
display();
break;
case 3:
insert_begin();
break;
case 4:
insert_end();
break;
case 5:
insert_pos();
break;
case 6:
delete_begin();
break;case 7:
delete_end();
break;
case 8:
delete_pos();
break;
case 9:
exit(0);
break;
default:
printf("\n\t Wrong Choice. Please valid choice(1/2/3/4/5/6/7/8/9)\n");
break;
}
}
return 0;
}
void create()
{
struct node *temp,*ptr;
temp=(struct node *)malloc(sizeof(struct node));
if(temp==NULL)
{
printf("\n\t Out of Memory Space!");
exit(0);
}
printf("\n\t Enter the data value for the node:");
scanf("\n\t %d",&temp->info);temp->next=NULL;
if(start==NULL)
{
start=temp;
}
else
{
ptr=start;
while(ptr->next!=NULL)
{
ptr=ptr->next;
}
ptr->next=temp;
}
}
void display()
{
struct node *ptr;
if(start==NULL)
{
printf("\n\t List is empty !");
return;
}
else
{
ptr=start;
printf("\n\t The List elements are:");
while(ptr!=NULL)
{printf("%d\t",ptr->info );
ptr=ptr->next ;
}
}
}
void insert_begin()
{
struct node *temp;
temp=(struct node *)malloc(sizeof(struct node));
if(temp==NULL)
{
printf("\n\t Out of Memory Space !");
return;
}
printf("\n\t Enter the data value for the node:" );
scanf("%d",&temp->info);
temp->next =NULL;
if(start==NULL)
{
start=temp;
}
else
{
temp->next=start;
start=temp;
}
}
void insert_end()
{struct node *temp,*ptr;
temp=(struct node *)malloc(sizeof(struct node));
if(temp==NULL)
{
printf("\n\t Out of Memory Space!");
return;
}
printf("\n\t Enter the data value for the node:" );
scanf("\n\t %d",&temp->info );
temp->next =NULL;
if(start==NULL)
{
start=temp;
}
else
{
ptr=start;
while(ptr->next !=NULL)
{
ptr=ptr->next ;
}
ptr->next =temp;
}
}
void insert_pos()
{
struct node *ptr,*temp;
int i,pos;
temp=(struct node *)malloc(sizeof(struct node));if(temp==NULL)
{
printf("\n\t Out of Memory Space:\n");
return;
}
printf("\n\t Enter the position for the new node to be inserted:");
scanf("\n\t %d",&pos);
printf("\n\t Enter the data value of the node:");
scanf("\n\t %d",&temp->info) ;
temp->next=NULL;
if(pos==0)
{
temp->next=start;
start=temp;
}
else
{
for(i=0,ptr=start;i<pos-1;i++) { ptr=ptr->next;
if(ptr==NULL)
{
printf("\n\t Position not found!");
return;
}
}
temp->next =ptr->next ;
ptr->next=temp;
}
}
void delete_begin()
{
struct node *ptr;
if(start==NULL)
{
printf("\n\t List is Empty !");
return;
}
else
{
ptr=start;
start=start->next ;
printf("\n\t The deleted element is :%d\t",ptr->info);
free(ptr);
}
}
void delete_end()
{
struct node *temp,*ptr;
if(start==NULL)
{
printf("\n\t List is Empty !");
return;
}
else if(start->next ==NULL)
{
ptr=start;
start=NULL;
printf("\n\t The deleted element is:%d\t",ptr->info);free(ptr);
}
else
{
ptr=start;
while(ptr->next!=NULL)
{
temp=ptr;
ptr=ptr->next;
}
temp->next=NULL;
printf("\n\t The deleted element is:%d\t",ptr->info);
free(ptr);
}
}
void delete_pos()
{
int i,pos;
struct node *temp,*ptr;
if(start==NULL)
{
printf("\n\t The List is Empty !");
return;
}
else
{
printf("\n\t Enter the position of the node to be deleted:");
scanf("%d",&pos);
if(pos==0){
ptr=start;
start=start->next ;
printf("\n\t The deleted element is:%d",ptr->info);
free(ptr);
}
else
{
ptr=start;
for(i=1;i<pos;i++) { temp=ptr; ptr=ptr->next ;
if(ptr==NULL)
{
printf("\n\t Position not Found!");
return;
}
}
temp->next =ptr->next ;
printf("\n\t The deleted element is:%d",ptr->info );
free(ptr);
}
}
}
