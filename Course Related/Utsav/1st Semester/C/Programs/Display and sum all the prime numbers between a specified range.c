#include <stdio.h>
#include <stdlib.h>
int check_prime(int num)
{
    if(num <= 2)
    {
        /* 0, 1 and 2 are prime or considered prime */
        return 1;
    }
    else
    {
        for(int i = 2; i < num; i++)
        {
            if(num % i == 0)
            {
                /* If any other number divides our target number we just return 0. Which will end the function */
                return 0;
            }
        }
        /* Since we went through all the numbers in the loop and no other number divides our target number so we just return 1 and end the function */
        return 1;
    }
}
int main()
{
    int a,b;
    printf("Enter range of number seperated by space: ");
    scanf("%d %d",&a, &b);
    int sum = 0;
    for(int i = a; i <= b; i++)
    {
        if(check_prime(i))
        {
            printf("%d\t",i);
            sum += i;
        }
        else
        {
            continue;
        }
    }
    printf("\nThe total sum of all the above prime numbers is %d",sum);
    return 0;
}