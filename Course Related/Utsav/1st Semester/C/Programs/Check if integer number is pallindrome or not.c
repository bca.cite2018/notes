#include <stdio.h>
int main()
{
    int num;
    printf("Enter the number: ");
    scanf("%d", &num);
    int backup_num = num; /* We save this number as a backup because we will modify num variable in the program but we still want what was there originally later down to check pallindrome */
    int reversed_num = 0;
    while(num > 0)
    {
        int last_digit = num % 10; /* It will take the last digit from num and put it in last_digit variable, but it will not remove it from num */
        reversed_num = reversed_num * 10 + last_digit; /* This is where we combine the last digit in a different variable */
        num /= 10; /* This is where remove the last digit from the num variable */
    }
    if(backup_num == reversed_num)
    {
        printf("The number is pallindrome");
    }
    else
    {
        printf("The number is not pallindrome");
    }
    return 0;
}