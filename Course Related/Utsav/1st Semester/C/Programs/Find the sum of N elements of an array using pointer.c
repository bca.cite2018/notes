/* May give error when running in visual studio under debug mode but it will not when running in release mode */

#include <stdio.h>
#include <stdlib.h>

void create_array(int* array, int size)
{
    /* Creates array of 'size' elements, if size=5, array would be {1,2,3,4,5} */
    for (int i = 0; i <= size; i++)
    {
        *(array + i) = i;
    }
}

int main()
{
    int N;
    printf("Enter the N: ");
    scanf("%d", &N);
    int* arr = (int*)malloc(N * sizeof(int));
    create_array(arr, N); /* Pass in malloced variable to the create_array function */
    int sum = 0;
    for (int i = 1; i <= N; i++)
    {
        sum += *(arr + i); /* Deference the (arr + i) address which will provide the value stored at that address. The value of i goes from 0 to the number of ints in the malloc */
    }
    printf("%d", sum);
    free(arr);
    return 0;
}