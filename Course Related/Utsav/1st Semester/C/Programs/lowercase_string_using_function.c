#include <stdio.h>
#include<stdlib.h>
#include<string.h>

char* lower(const char* string, int length) // Array is returned as pointer. No mechanism to easily count lenght from in here so it must also be passed
{
    char* new_string = (char*)malloc(sizeof(char)); // Ahhh its C being C, cant just return an array from a function without using heap allocation
    const int difference_factor = 97 - 65; //ASCII value of A is 65 and a is 97.
    int i;
    for(i = 0; i < length; i++)
    {
        char char_at_hand = *(string + i);
        if(char_at_hand >= 65 && char_at_hand <= 90)
        {
            char_at_hand += difference_factor;
            *(new_string + i)= char_at_hand;
        }
        else
        {
            *(new_string + i) = char_at_hand;
        }
    }
    *(new_string + i) = '\0'; // Not adding Null terminator at the end will start giving weird results
    return new_string;
}

int main(int argc, char *argv[])
{
    char* fullname;
    printf("Enter your full name: ");
    scanf("%[^\n]s", fullname); // Using &fullname causes segmentation fault
    char* lowercased = lower(fullname, strlen(fullname));
	printf("%s",lowercased);
	free(lowercased); // Must free Malloced memory remember?
}