/* Written in Codeblocks IDE */
/* It is better to disable word-wrap if you are opening this in non-IDE text editor */

#include <stdio.h>
#define criteria 0.001 /* We want to correct upto three decimal places */

float convert_into_positive(float x)
{
      if(x < 0)
      {
          x *= -1; /* When we multiply a negative number by -1, the answer is positive */
          return x;
      }
      else
      {
          return x;
      }

      /* In C, there is a function called fabs() inside math.h header file that quickly converts a number to its absolute value */
}

float function(float x)
{
    return x*x -4*x -10;
   /* x*x can be written as pow(x,2) if you import math.h header file */
}

float derivative_of_function(float x)
{
    return 2*x - 4;
}

float newton_rapshion_formula(float x0) /* This function calculates newton rapshion formula */
{
    return x0 - (function(x0)/derivative_of_function(x0)); /* This function calculates newton rapshion formula */
}

void main()
{
    int truth = 1; /* We initially set this to 1 so that the first while block will run. In C, we dont have boolean datatype, so we use 1 and 0 to indicate true or false */
    float x7;
    while(truth)
    {
        printf("Enter the guess for x1: ");
        scanf("%f", &x7);
        if(derivative_of_function(x7) == 0)
        {
            printf("\nPutting x1 to derivate of the function results to 0, try again\n");
            truth = 1; /* Here, we set truth to 1 so that the while block will run again */
        }
        else
        {
            truth = 0; /* Once our condition is satisfied, we break out of while block by setting truth to 0. Here, the while block wont run again as 0 indicates false */
        }

    }

    printf("\n--------------------------------------------------\n");
    float x1;
    int step = 0; /* This is used to count step */
    while(convert_into_positive(function(x7)) >= criteria)
    {
        step += 1;
        x1 = newton_rapshion_formula(x7);
        x7 = x1;


        printf("Step:%d x1=%f function(x1)=%f derivative_of_function(x1)=%f\n",step,x1,function(x1),derivative_of_function(x1));

    }
    printf("--------------------------------------------------\n");
    float output = function(x1); /* The output is the value of function(x1) at the last step */
    printf("The output is %f which took %d steps to calculate",output,step);
}
