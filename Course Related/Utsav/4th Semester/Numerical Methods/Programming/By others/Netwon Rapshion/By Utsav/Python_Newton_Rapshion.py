#Written in Thonny IDE
#It is better to disable word-wrap if you are opening this in non-IDE text editor
truth = True #We initially set this to True so that the first while block will run
criteria = 0.001 #We want to correct upto three decimal places

def convert_into_positive(x):
    if x < 0: #When we multiply a negative number by -1, the answer is positive
        x *= -1
        return x
    else:
        return x
    #In python, there is a function called abs() inside math module that quickly converts a number to its absolute value

def function(x):
    return x*x - 4*x - 10
    #x*x can be written as pow(x,2) if you import math module

def derivative_of_function(x):
    return 2*x - 4

def newton_rapshion_formula(x0): #This function calculates newton rapshion formula
    return x0 - (function(x0)/derivative_of_function(x0))
    


while truth:
    x0 = float(input("Enter the guess for x1: "))
    if derivative_of_function(x0) == 0:
        print("Putting x1 to derivate of the function results to 0, try again")
        truth = True #Here, we set truth to true so that the while block will run again
    else:
        truth = False #Once our condition is satisfied, we break out of while block by setting truth to False. Here, the while block wont run again
        
print("--------------------------------------------------")
step = 0 #This is used to count step

while convert_into_positive(function(x0)) >= criteria:
    step += 1
    x1 = newton_rapshion_formula(x0)
    x0 = x1
    
    print("Step: "+str(step)+" x1= "+str(x1)+" function(x1)= "+str(function(x1))+" derivative_of_function(x1)= "+str(derivative_of_function(x1)))

print("--------------------------------------------------")
output = function(x1) #The output is the value of function(x1) at the last step
print("The output is "+str(output)+" which took "+str(step)+" steps to calculate")
