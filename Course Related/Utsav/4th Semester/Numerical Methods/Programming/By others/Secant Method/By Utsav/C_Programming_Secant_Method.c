/* Written in Codeblocks IDE */
/* It is better to disable word-wrap if you are opening this in non-IDE text editor */
#include <stdio.h>
#define criteria 0.001 /* We want corrected upto 3 decimal places */

float function(float x)
{
    return x*x-5*x+6;
   /* x * x can be written as pow(x,2) if you import math.h header file */
}

float convert_into_positive(float x)
{
    if(x < 0)
    {
        x *= -1; /* When we multiply a negative number by -1, it becomes positive */
        return x;
    }
    else
    {
        return x;
    }
    /* In C, there is a function called fabs() inside math.h header file that quickly converts a number to its absolute value */
}

float secant_formula(float x1, float x2) /* This function is to calculate secant formula */
{
    return x2-function(x2)*(x2 - x1)/(function(x2) - function(x1));
}

void main()
{
    float x1,x2;
    printf("Enter x1 and x2: ");
    scanf("%f %f",&x1,&x2);
    /* In secant method, we dont need to check for anything with two initial guesses */
    int step = 0;
    float x3 = 0;
    printf("\n-------------------------\n");
    while(convert_into_positive(function(x3)) >= criteria)
    {
        step += 1;
        x3= secant_formula(x1,x2);
        x1 = x2;

        printf("Step:%d x1=%f x2=%f x3=%f function(x1)=%f function(x2)=%f function(x3)=%f\n",step,x1,x2,x3,function(x1),function(x2),function(x3));

    }

    printf("\n-------------------------\n");
    float output = x3; /* The answer is in x3 at the last step */
    printf("One of the root is %f which took %d steps to find out",output,step);

}
