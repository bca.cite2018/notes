#Written in Thonny IDE
#It is better to disable word-wrap if you are opening this in non-IDE text editor
criteria = 0.001 #We want corrected upto 3 decimal places

def function(x):
    return x*x -5*x +6
    #x * x can be written as pow(x,2) if you import math module

def convert_into_positive(x):
    if x < 0:
        x *= -1 #When we multiply a negative number by -1, it becomes positive
        return x
    else:
        return x
    #In python, there is a function called abs() inside math module that quickly converts a number to its absolute value
    
def secant_formula(x1,x2): #This function is to calculate secant formula
    return x2-function(x2)*(x2 - x1)/(function(x2) - function(x1))
    
x1 = float(input("Enter for x1: "))
x2 = float(input("Enter for x2: "))
#In secant method, we dont need to check for anything with two initial guesses

step = 0 #This is used later to count the steps
x3 = 0
print("-------------------------")

while(convert_into_positive(function(x3)) >= criteria):
    step += 1
    x3= secant_formula(x1,x2)
    x1 = x2
    print("Step: "+str(step)+" x1="+str(x1)+" x2="+str(x2)+" x3="+str(x3)+" function(x1)="+str(function(x1))+" function(x2)="+str(function(x2))+" function(x3)="+str(function(x3)))
print("-------------------------")
output = function(x3) #The answer is in function(x3) at the last step
print("One of the root is "+str(output)+" which took "+str(step)+" steps to find out")

