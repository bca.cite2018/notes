"""
1: Define f(x)
2:Input a,b,n
3: Calculate h = (b-a)/n
4: Calculate I = (h/2)(f(a)+f(b)+2(summatio

"""
def f(x):
    """Function of x"""
    return 1/x

a = float(input("Enter a: "))
b = float(input("Enter b: "))
n = int(input("Enter n: "))
h = (b - a)/n
sum = 0.0

for i in range(1,n):
    sum = sum + f(a + i*h)
    sum = h/2 * (f(a) + f(b) + 2*sum)
print(f"I = {sum}")