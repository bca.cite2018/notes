#include<stdio.h>
#include<math.h>
float fx(float x)
{
    return x*x-4*x-10;
}
void main()
{

    float x1,x2,x3, fx1, fx2, fx3;
    int i;
    printf("Enter two initial guess fx1 and fx2:");
    scanf("%f%f", &x1, &x2);
    while((fx(x1)*fx(x2))>0)
    {
    printf("\nSince fx1 and fx2 are of same sign try again::");
    scanf("%f%f", &x1, &x2);
    }
    i=0;
    printf("\tno\tx3\tf(x3):");

    while(fabs(fx(x3))>=0.001)
    {
        i=i+1;
        x3=(x1+x2)/2;
        fx3=fx(x3);
        if(fx(x3)*fx(x1)>0)
        {
            x1=x3;
        }
        else if(fx(x3)*fx(x2)>0)
        {
            x2=x3;
        }
        else if(fx(x3)==0)
        {
            x1=x3;
            x2=x3;
        }
        printf("\n \t%d\t%f\t%f\n", i, x3, fx3);
    }
    printf("\n \t%d\t%f\t%f\n", i, x3, fx3);

}
