#Written in Thonny IDE
#If you are using a non-IDE text editor, it is better to disable word wrap
#One indentation equals four spaces
criteria = 0.001 #We need the answer corrected upto 3 decimal places


x3 = 0       #Creating a variable x3 and setting its initial value to 0. It will come later
truth = True #Setting truth to True so that first while block will run

def function(x): #This function returns a value when 'x' is put in function
    return x*x-4*x-10
    #x*x can be written as pow(x,2) if you import math module
    
def same_sign_check(x1,x2): #This function is to check if x1 and x2 have same sign or not
    if(x1 * x2 > 0): #When we multiply two numbers and the answer is positive, it means those two numbers had the same sign
        return True
    else:
        return False
    

def bisection_formula(x1,x2): #This function is to calculate bisection
    return (x1 + x2)/ 2

def convert_into_positive(x):
    if x < 0:
        x = x * -1 #When we multiply a negative number by -1, it becomes positive and absolute
        return x
        #In python, there is a function called abs() inside math module that quickly converts a number to its absolute value
    else:
        return x


while truth: #We start an infinite loop and break out when condition is favorable. At first, truth = True so this while block will run
    x1 = float(input("Enter x1: "))
    x2 = float(input("Enter x2: "))
    if not(same_sign_check(function(x1),function(x2))): #If same sign check is not True
        truth = False  #When the condition is favorable, we break out of the loop by setting it false. Here, we set truth to False such that the while block wont run
    else:
        print("They are of same sign, please try again")
        #If the condition is not favorable, we print the message and continue to while block
        truth = True #if they are of same sign, we set truth to True so that the while block runs again. It is not necessary but will help make things easier to understand


print("-----------------------------------------------")
step = 0 #This is used to count the steps

while (convert_into_positive(function(x3)) >= criteria):
    step += 1
    
    x3 = bisection_formula(x1,x2) #My first mistake was not putting this inside this while block
    if same_sign_check(function(x1),function(x3)): #If function(x1) and function(x3) have same sign
        x1 = x3
       
        
    elif same_sign_check(function(x2),function(x3)):
        x2 = x3
        
        
    elif function(x3) == 0:
        x1 = x3
        x2 = x3
    
    print("Step:"+str(step)+", x1 ="+str(x1)+", x2="+str(x2)+", x3="+str(x3)+" and function(x3)="+str(function(x3))+".\n")
        
        
print("-----------------------------------------------")
output = function(x3) #Answer is obtained when we look at the value of function(x3) at the last step
print("One of the root is "+str(output)+" that took "+str(step)+" steps to find out.") #Python doesnt allow concatinating numbers with strings so we need to convert numbers into string before concatinating
