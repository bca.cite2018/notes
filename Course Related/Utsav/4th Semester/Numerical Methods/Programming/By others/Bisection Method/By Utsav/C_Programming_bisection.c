/* Written in Codeblocks IDE */
/* If you are using a non-IDE text editor, it is better to disable word wrap */
#define criteria 0.001  /* We need the answer corrected upto 3 decimal places */
#include <stdio.h>


float function(float x)
{
    return x*x-4*x-10; /* This function returns a value when 'x' is put in function */
    /* x*x can be written as pow(x,2) if you include math.h header file */
}



int same_sign_check(float x1, float x2) /* This function is to check if x1 and x2 have same sign or not */
{
    if(x1*x2 > 0) /* When we multiply two numbers and the answer is positive, it means those two numbers had the same sign */
    {
        return 1; /* In C we dont have booleans to check true or false so we use 0 and 1 to indicate false or true */
    }

    else
    {
        return 0;

    }
}



float bisection_formula(float x1, float x2) /* This function is to calculate bisection */
{
    return ((x1 + x2)/2);
}



float convert_into_positive(float x)
{
    if(x < 0)
    {
        x = x * -1; /* When we multiply a negative number by -1, it becomes positive and absolute */
        return x;
        /* In C, there is a function called fabs() inside math.h header file that quickly converts a number to its absolute value */
    }

    else
    {
        return x;
    }

}



void main()
{
    int truth = 1; /* Setting truth to 1 so that first while block will run */
    float x1,x2,x3; /* Declaring all these variables */

    while(truth) /* We start an infinite loop and break out when condition is favorable. At first, truth = 1 so this while block will run */
    {
        printf("Please enter x1 and x2: ");
        scanf("%f %f", &x1, &x2);
        if (!(same_sign_check(function(x1),function(x2)))) /* If same sign check is not True */
        {
            truth = 0;
            /* When the condition is favorable, we break out of the loop by setting it 0. Remember that 0 is false. Here, we set 1 to 0 such that the while loop wont run */
        }

        else
        {
            printf("\nThey are of same sign, try again, \n");
            /* If the condition is not favorable, we print the message and continue to while block */
            truth = 1; /* if they are of same sign, we set truth to 1 so that the while block runs again. It is not necessary but will help make things easier to understand */

        }

    }

    int step = 0; /* This is used to count the steps */
    printf("\n-----------------------------------------------\n");
    while(convert_into_positive(function(x3))>= criteria)
    {
        step += 1;
        x3 = bisection_formula(x1,x2); /* My first mistake was not putting this inside this while block */
        if(same_sign_check(function(x1),function(x3))) /* If function(x1) and function(x3) have same sign */
        {
            x1 = x3;
        }

        else if(same_sign_check(function(x2),function(x3)))
        {
            x2 = x3;
        }

        else if(function(x3) == 0)
        {
            x1 = x3;
            x2 = x3;

        }

        printf("\nStep:%d, x1=%f, x2=%f, x3=%f, function(x3)=%f\n",step,x1,x2,x3,function(x3));


    }

    printf("\n-----------------------------------------------\n");
    float output = function(x3); /* Answer is obtained when we look at the value of function(x3) at the last step */
    printf("\nOne of the root is %f that took %d steps to find out.\n",output,step);

}
