<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>HTML Validation</title>
    <style>
        /* Start CSS */
        form {
            display: grid;
            grid-template-columns: auto 1fr;
            gap: 5px;
        }
        form > button {
            width: 5em;
        }
        /* End CSS */
    </style>
</head>
<body>
    <h2>User Registration</h2>
    <form action="#" method="POST">
        <label>First Name:</label>
        <input type="text" id="firstName" name="firstName">
        <label>Last Name:</label>
        <input type="text" id="lastName" name="lastName">
        <label>Email:</label>
        <input type="text" id="email" name="email">
        <label>Password:</label>
        <input type="password" id="password" name="password">
        <button type="reset">Reset</button>
        <button id="send" type="submit" name="submitbutton">Submit</button>
    </form>
</body>
</html>
<script>
    /* Start Javascript */
    function onSubmitClick()
    {
        const firstName = document.getElementById("firstName").value;
        const lastName = document.getElementById("lastName").value;
        const email = document.getElementById("email").value;
        const password = document.getElementById("password").value;
        // Client side validation
        if(firstName === "" || lastName === "" || email === "" || password === "") 
        {
            alert("Please fill in all the fields.");
        } 
        else 
        {
            if(!(email.includes("@")))
            {
                alert("Please enter a valid email");
            }
        }
    }
    const form = document.querySelector("form");
    form.addEventListener('submit', onSubmitClick);
</script>

<?php
    if (isset($_POST['submitbutton']))
    {
        $firstName = $_POST['firstName'];
        $lastName = $_POST['lastName'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        // Server side validation
        if(isset($firstName) && isset($lastName) && isset($email) && isset($password))
        {
            if(strpos( $email, "@" ))
            {
                $conn = new mysqli("localhost", "root", "", "userdb");
                if($conn -> connect_errno)
                {
                    echo("$conn->connect_errno");
                }
                $insert_query = "insert into users(firstName, lastName, email, password) values('$firstName', '$lastName', '$email', '$password')";
                $value = $conn -> query($insert_query);
                if($value == TRUE)
                {
                    echo("New record added");
                }
                else
                {
                    echo("error:{$conn->error}");
                }
            }
        }
    }

    // SQL used to generate table:
    // create table users
    // (
    //     id SERIAL,
    //     firstName VARCHAR(20),
    //     lastName VARCHAR(20),
    //     email VARCHAR(20),
    //     password VARCHAR(20),
    //     PRIMARY KEY(id)
    // );
?>


