======================
# 2.1  unified modelling language(uml)

- the unified modelling language is a graphical language for OOAD that gives standard way to write a software system's blueprint.
- it helps to visualize, specify, construct and document the artifact of an object oriented system.
- it used to depict the structure and the relationship in a complex system.
- the goal of uml is to provide a standard notation that can be used by all object oriented methods and to select and integrate the best elements of precursor notation.

## history

- it was developed 1990s as an amalgamation of several techniques, prominently OOAD technique by grady booch, omt(object modelling technique) by james rumbaugh and oose(object oriented software engineering) by ivar jacobson.

- uml attempted to standardize semantic models, syntactic notations and diagrams of OOAD

1. object oriented technique omt [james rumbaugh 1991 ] was best for analysis and data intensive information system.

2. booch [gray booch 1994] was excellent for design and implementation. grady booch had worked extensively with the ada language and had been a major player in the development of object oriented techniques for the language. although, the booch method was strong, the notation was less well received.

3. oose [object oriented software engineering ivar jacobson 1992] featured a model known as use cases. use cases are powerful technique for understanding the behaviour of an entire system.

# 2.2 notation and basic building blocks of uml

graphical notations used in structural things are most widely used in uml

## structural things:

1. class notation

- the top section used to name the name.
- the second one is used to show the attributes of the class.
- the third section is used to describe the operations performed by the class.
- the fourth section is optional solution to show any additional components

[class diagram here]

- classes are used to represent objects. objects can be anything having properties and responsibilities

2. object notation

- the object is represented in the same way as the class. the only difference is the name which is underlined.

[object diagram here]

- as the object is an actual implementation of a class

3. interface notation

- interface is represented by a circle

[interface diagram here]

- interface is used to describe the functionality without implementation.
- interface is just like a template, where you define functions.
- when a class implements the interface, it also implements the functionality as per requirements

4. collaboration notation

- use case is represented by a dotted eclipse.
- generally responsibilities are in group.

[collaboration diagram here]

5. use case notation

- use case is represented as an eclipse with a name inside it
- it may contain additional responsibilities
- use case is used to capture light level functionalities of a system

6. actor notation

- an actor can be defined as some internal or external entities that interacts with the system
- an actor is used in use case diagram to describe the internal or external entities
[actor diagram here]

7. initial state notation

- initial state is defined to show the start of a process
- the usage of initial state notation is to show the starting point of a process
[initial state notation diagram here]

8. final notation

- final state is used to show the end of the process. this notation is also used in almost all diagrams to describe the end
- the usage of final notation is to show the termination point of a process
[final notation diagram here]

9. active class notation

- active class looks similar to a class with a solid border. active class is generally used to describe the concurrent behaviour of a system
- active class is used to represent the concurrency in a system


10. component notation

- a component notation is used to represent any part of a system for which uml diagrams are made.
- additional elements can have added wherever required


11. node notation

- node notation is used to represent the physical part of a system such as the server, network, etc.
- a node can represent the physical component of the system.

## behavioral things:
- dynamic parts are one of the most important elements in uml. uml has a set of powerful features to represent the dynamic part of software and non software system

1. interaction notation

- interaction notation is basically a message exchange between two uml components.
- interaction notation is used to represent the communication among the components of a system
[interaction notation diagram here]


2. state machine notation

- state machine describes the different state of a component in its life cycle.
- it is used to describe different state of a system components.
- the state can be active, idle or any other depend upon the situation 
[state machine diagram here]


======================
# 2.3 uml diagram

uml stands for unified modelling language. its a rich language to model software solutions, application structure, system behaviour.

there are 14 uml diagrams to help you model the system behaviour. there are two main categories; structure diagram and behavioural diagram

Structural diagram:
1. class diagram
2. component diagram
3. deployment diagram
4. object diagram
5. package diagram
6. profile diagram
7. composite structure diagram

Behavioural diagram:
1. state machine diagram
2. communication diagram
3. use case diagram
4. activity diagram
5. sequence diagram
6. timing diagram
7. interaction diagram

======================
# 2.4 extend, generalization, specification

# 2.4.1 extend relationship

- in uml modelling, we can use an extended relationship to specify that one use case (extension) extends the behavior of another use case (base). this type of relationship reveals details about a system or application that are typically hidden in a use case.
- the extend relationship specifies that the incorporation of the extension use is dependent on what happens when the base use case executes.
- while the base use case is defined independently and is meaningful by itself, the extension use case us not meaningful on its own.


we can add extend relationships to a model to show the following situations:

1. a part of a use case that is optional system behaviour.
2. a subflow is executed only under certain conditions.
3. a set of behaviour segments that may be inserted in a base use case
[base use case figure here]

for example:
you are developing an ecommerse system in which you have a base use case called placeonlineorder that has an extending use case called specify shipping instructions. an extend relationship point from from the specify shipping instruction use case to placeonlineorder use case to indicate that the behaviours in the specify shipping instructions use case are optional and only occur in certain circumstances

# 2.4.2 generalization

- in uml modelling, a generalization relationship that implements the concept of object oriented called inheritance.
- the generalization relationship occurs between two entities or objects such that one entry is the parent, and the other is child. the child inherits the functionalities of its parents and can access as well as update it.
- generalization relationship is utilized in class, component, deployment and use case diagram to specify that the child inherits actions, characteristics and relationship from its parents
- The Generalization relationship is incorporated to record attributes, operations and relationships in a parent model element so that it can be inherited in one or more child model elements.


# 2.4.3 Instance of Specification in UML

- In UML models, instance specification are events that represent an instance in the modeled system.
- When you have new instance of classified models, the instance specification that you create represent an entity in the modeled system at a point in time, similar to a snapshot of the entity.
- Instance specification can include the following information about the entity:
	- The classification of the entity by one or more classifiers of which the entity is an instance.
	- The kind of instance, based on its classifier, for example, an instance specification whose classifier is an association describes a link to the association.
	- Specific value of the structural features of the entity, which are represented by slots.


======================
# 2.5: Object oriented Development Life cycle(Iterative and incremental life cycle (VVI)

# 2.5.1: Iterative Model:

- In the iterative model, iterative process starts with a simple implementation of small set of software requirements and iteratively enhances the evolving versions until the complete system is implemented and ready to deploy.
- An iterative life cycle model does not attempt to start with a full specification requirement, instead, development begins by specifying and implemented just part of software, which is reviewed to identify further requirements.
- This process is then repeated, producing a new version of the software at the end of each iteration of the model.

[Iterative model diagram here]

Applications:
    - Requirements of the complete system are clearly defined and understood.
    - Major requirements must be defined, however, some functionalities or requested enhancements may evolve with time.
    - There is a time to the market constraints.
    - A new technology is being used and is being learnt by the development team while working on the project.
    - Resources with needed skill sets are not available and are planned to be used on contract basis for specific iterations.
    - There are some high risk features and goals which may change in the future.

Advantages and Disadvantages
    
    Advantages:
      
      1. Some working functionality can be developed quickly and early in the life cycle.
      2. Results are obtained early and periodically
      3. Parallel development can be planned
      4. Progress can be measured.
      5. Less costly to change the scope/requirement
      6. Testing and debugging during smaller iteration is easy.
      7. Manage risk
      8. Operational product delivered
      9. Risk analysis is better

    Disadvantaged
      1. More resources may be required
      2. Cost if change is lesser but is not very suitable for changing environment
      3. More management attention is required
      4. Not suitable for smaller projects
      5. Management complexity is higher

======================
# 2.5.2: Incremental Model

- Incremental model is a process of software development where requirements are broken down into multiple standalone modules of software development cycle.
- Incremental development is done in steps from analysis, design, implementation, testing/verification, maintenance.
- The system is put into production when the first increment is delivered. The first increment is often a core product where the basic requirements are addressed, and supplementary features are added in the next increments.
- Once the core product is analyzed by the client, there is a plan development for the next increment.

Characteristics:
  
  - System development is broken down into many mini development projects.
  - High priority requirements is tackled first
  - Once the requirement is developed, requirements for that increment are frozen.
  - Produce a final system

When to use Incremental models?
  - Requirements of the system are clearly understood.
  - When demand for an early release of a product arises
  - When software engineering team are not very well skilled or trained.
  - High risk features and goals are involved.
  - Such methodology is more in user for web application and product based companies.

Advantages and disadvantages:
    
    Advantages
      
      1. The software will be generated quickly during the software life cycle.
      2. Flexible and less expensive to change requirements and scope.
      3. Development stages can be done
      4. Less costly compared to others
      5. A customer can respond to each building
      6. Errors can be easily identified

    Disadvantages
    
      1. It requires a good planning designing
      2. Problems might cause due to system architecture as such not all requirements collected up front for the entire software cycle.
      3. Does not overlap each other while iteration phase is rigid.
      4. All the units consume a lot of time

 
