#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/utsname.h> //header file for structure utsname

int main(int argc,char *argv[]) 
{
	int z;
	struct utsname u_name; //utsname is the system defined structure

	z = uname(&u_name);

	if ( z == -1 ) 
	{
		fprintf(stderr, "%s: uname()\n",strerror(errno));
		exit(1);
	}

	printf(" sysname[]   = '%s';\n", u_name.sysname); //Operating system name
	printf(" nodename[]  = '%s';\n", u_name.nodename); //Name within "some implementation-defined network" 
	printf(" release[]   = '%s';\n", u_name.release); //Operating system release
	printf(" version[]   = '%s';\n", u_name.version); //Operating system version
	printf(" machine[]   = '%s';\n", u_name.machine); //Hardware identifier

	return 0;
}
