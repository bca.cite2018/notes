//write a program to implement fifo()
//both the program can read and write in fifo file
//do not need to call mkfifo due to it is created by program1

#include<stdio.h>
#include<unistd.h>
#include<fcntl.h>

void main()
{
	int fd;
	char rdbuffer[80],wrbuffer[80];

	while(1)
	{	
		fd=open("myfile",O_WRONLY);	
		printf("please write the text p2:");	
		fgets(wrbuffer,sizeof(wrbuffer),stdin);
		write(fd,wrbuffer,sizeof(wrbuffer));
		close(fd);
		
		fd=open("myfile",O_RDONLY);
		read(fd,rdbuffer,sizeof(rdbuffer));
		printf("From program1:%s",rdbuffer);
		close(fd);
	}

}
