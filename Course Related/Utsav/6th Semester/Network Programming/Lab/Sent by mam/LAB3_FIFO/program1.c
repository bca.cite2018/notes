//write a program to implement fifo()
//both the program can read and write in fifo file

#include<stdio.h>
#include<unistd.h>
#include<sys/stat.h>
#include<fcntl.h>

void main()
{
	int fd;
	char rdbuffer[80],wrbuffer[80];
	char * myfile;
	
	mkfifo("myfile",0666); //you can make separate program for creating the fifo file.		
	while(1)
	{	
		fd=open("myfile",O_RDONLY); //open fifo file in read mode
		read(fd,rdbuffer,sizeof(rdbuffer));	
		printf("From program2: %s",rdbuffer);
		close(fd);
		
		fd=open("myfile",O_WRONLY); //open fifo file in write mode
		printf("please write the text to be write in fifo file:");
		fgets(wrbuffer,sizeof(wrbuffer),stdin);
		write(fd,wrbuffer,sizeof(wrbuffer));
		close(fd);
	}	

}
