//write a program to implement pipe()
//Child process write the content in one end of pipe
//Parent process read that content from other end of pipe

#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>


int main()
{
	int mypipe[2],mypid;
	char buffer[50];

	if (pipe(mypipe)==-1)
	{
		perror("pipe could not be created.");
		exit(1);
	}
	else
	{
		mypid=fork();
		
		if(mypid<0)
		{
			perror("child process could not be created.");
			exit(1);
		}
		else if(mypid==0)
		{
			//im in child process
			close(mypipe[0]); //close the read end of pipe.
			write(mypipe[1],"this is lab second",14);
			
		}
		else
		{
	
			//im in parent process
			close(mypipe[1]);
			read(mypipe[0],buffer,sizeof(buffer));
			printf("the child process written is: %s",buffer);
		}

	}
	return 0;
}
