#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<string.h>
#include<netdb.h>

int main(int argc, char * argv[])
{


	int sockfd,bindfd,fromlen,n;
	struct sockaddr_in server;
	struct sockaddr_in from;
	char buff[50];
	
	if(argc<2)
	{
		printf("insufficient argument");
		exit(1);
	}	
	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		printf("could not create the socket.");
		exit(1);
	}
	memset((char*)&server,0,sizeof(server));
	server.sin_family=AF_INET;
	server.sin_port=htons(atoi(argv[1]));
	server.sin_addr.s_addr=INADDR_ANY;
	bindfd=bind(sockfd,(struct sockaddr *)&server,sizeof(server));
	if(bindfd<0)
	{
		printf("could not bind.");
	}
	fromlen=sizeof(from);
	while(1)
	{
		n=recvfrom(sockfd,buff,sizeof(buff),0,(struct sockaddr *)&from,&fromlen);
		if(n<0)
		{
			printf("No data to receive.");
		}
		printf("Message received from client is:%s",buff);

		printf("type the response to be sent:");
		fgets(buff,sizeof(buff),stdin);		
		n=sendto(sockfd,buff,sizeof(buff),0,(struct sockaddr *)&from,fromlen);
		if(n<0)
		{
			printf("could not send message.");
		}
	
	}
	return 0;
}
