#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<string.h>
#include<netdb.h>
int main(int argc, char * argv[])
{


	int sockfd,bindfd,fromlen,n;
	struct sockaddr_in server, from;
	struct hostent *hp;
	char buff[50];
	
	if(argc<3)
	{
		printf("insufficient argument");
		exit(1);
	}	

	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		printf("could not create the socket.");
		exit(1);
	}
	
	server.sin_family=AF_INET;
	server.sin_port=htons(atoi(argv[2]));
	hp=gethostbyname(argv[1]);
	memcpy((char*)hp->h_addr,(char  *)&server.sin_addr.s_addr,hp->h_length);
	
	fromlen=sizeof(server);

	printf("Enter the value to request the server:");
	fgets(buff,sizeof(buff),stdin);
	n=sendto(sockfd,buff,sizeof(buff),0,(struct sockaddr *)&server,fromlen);
	if(n<0)
	{
		printf("could not send message.");
	}
	n=recvfrom(sockfd,buff,50,0,(struct sockaddr *)&server,&fromlen);
	if(n<0)
	{
		printf("No data to receive.");
	}
	printf("Response sent by server is:%s",buff);	
	return 0;
}
