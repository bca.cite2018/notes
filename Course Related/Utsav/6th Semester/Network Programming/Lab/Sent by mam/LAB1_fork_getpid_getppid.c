//Write a program to display parent and child process id.
//if you remember in our lab session, getppid() always returns 1
//Because parent process is finished by the time the child asks for its parent's pid.

#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>

int main()
{
	char fd[50];
	pid_t mypid;
	int ret;

	mypid=fork();
	if (mypid < 0)
	{
		printf("child process could not created.")
		exit(1);
	}
	if(mypid==0)
	/*im at child process */
	{
		printf("child process id is: %d  and parent process id is %d:", getpid(), getppid());
			
	}
	else
	{
	/* im at parent process */		
		printf("parent process id is: %d  and child process id is %d:", getpid(), mypid);
		
	}	
	return 0;
}

