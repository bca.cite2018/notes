//TCP client/server programming 

#include<stdio.h>
#include<sys/socket.h>
#include<string.h>
//#include<sys/types.h>
#include<netinet/in.h>	//...for htons,INADDR_ANY
#include<stdlib.h>
#include<unistd.h>


int main(int argc, char *argv[])
{
	int sockfd, newsockfd, bindfd,portno,clilen;
	char buffer[256];
	int buf;	
	struct sockaddr_in server, cli_addr;

	if(argc<2)
	{
		perror("Not provided all the information.");
		exit(1);
	}

	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)
	{
		perror("could not create socket.");
		exit(1);
	
	}

	memset((char *)&server, 0, sizeof(server));

	server.sin_family=AF_INET;
	server.sin_addr.s_addr=INADDR_ANY;

	portno=atoi(argv[1]);
	server.sin_port=htons(portno);

	bindfd=bind(sockfd, (struct sockaddr *)&server, sizeof(server));
	if(bind<0)
	{
		perror("could not bind.");
		exit(1);
	}

	listen(sockfd,5);

	clilen=sizeof(cli_addr);
	newsockfd=accept(sockfd,(struct sockaddr *)&cli_addr,&clilen);
	if(newsockfd<0)
	{
		perror("could not accept client.");
		exit(1);
	}

	memset(buffer,0,sizeof(buffer));

	buf=read(newsockfd,buffer,256);
	if(buf<0)
	{
		perror("reading from socket.");
		exit(1);
	}

	printf("the value is %s\n",buffer);
	buf=write(newsockfd,"this is for chapter 5",25);
	if(buf<0)
	{
		perror("error in writing.");
		exit(1);
	}
	return 0;
}

