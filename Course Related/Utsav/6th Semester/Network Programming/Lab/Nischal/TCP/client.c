#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  // create a socket
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  // define ip & port to socket
  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(4444);
  server_address.sin_addr.s_addr = INADDR_ANY;
  // Connection
  int conn =
      connect(sock, (struct sockaddr *)&server_address, sizeof(server_address));
  // error check
  if (conn == -1) {
    printf("Error connecting the server.\n");
  }
  char smsg[256];
  char cmsg[256];
  while (1) {
    // Receive message
    recv(sock, &smsg, sizeof(smsg), 0);
    printf("Server: %s \n", smsg);
    // Send message
    printf("You: ");
    scanf("%s", cmsg);
    send(sock, cmsg, sizeof(cmsg), 0);
  }
  // close connection
  close(sock);
  return 0;
}
