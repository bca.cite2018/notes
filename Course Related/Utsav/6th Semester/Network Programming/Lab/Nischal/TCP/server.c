#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  // create the socket
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  // define ip & port
  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(4444);
  server_address.sin_addr.s_addr = INADDR_ANY;
  // bind ip & port to socket
  bind(sock, (struct sockaddr *)&server_address, sizeof(server_address));
  // listen for connections
  listen(sock, 5);
  // accept the connection
  int client = accept(sock, 0, 0);
  char smsg[256];
  char cmsg[256];
  while (1) {
    // Send message
    printf("You: ");
    scanf("%s", smsg);
    send(client, smsg, sizeof(smsg), 0);
    // Receive message
    recv(client, &cmsg, sizeof(cmsg), 0);
    printf("Client: %s \n", cmsg);
  }
  // close connection
  close(sock);
  return 0;
}
