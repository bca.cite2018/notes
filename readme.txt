-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256



# Whats up with this weird blob of text surrounding this text?
    - That exists because this file was signed with the key referenced below. It helps us to prove these files are indeed coming from us and was not modified. You can ignore those if you want.

# How are files organized?
    - Everything course related is in Course Related folder. There may be different people maintaining/contributing files in There
    - Anything else that we think is worth sharing is in the Miscellaneous folder

# What about Notes for BIT.txt
    - Resources in there are referred to and recommended by our friends who study BIT
    - We do not maintain that site, but we recommend that site for those looking for BIT notes

# Is this for TU or PU?
    - This is for 4 years Purbanchal University BCA course
    - We are of 2018 batch

# How valid is the course syllabus?
    - Well, The files here are based on Purbanchal University syllabus from year 2014 AD to 2019 AD after which it was changed again
    - Although, not many things were changed. So you can still make use of files in here

# Can I get link to the notes?
    - Have a look at mirrors.txt file in the same folder

# What subjects are included?
    - Please refer to Which_Subject_in_Which_Semester.txt for a full list

# Some notes/question papers are missing
    - We try as much as we can to share as much as we have, but unfortunately we sometimes do not have a full set of everything necessary
    - We are always updating this cloud drive and are always adding more files in here.
    - We also accept help from others. If you have any missing resources, please send it to us.

# Where can I find other files such as Registration numbers?
    - It was removed from this link and instead only available in the other SecretShared folder for privacy reasons
    - Anything that has personal information of the people are in the SecretShared folder, please contact us if you have a valid reason to have it

# How can I contact you?
    - You can use the email: public@bca2018.anonaddy.com or publicbca2018.oreortyx@simplelogin.fr
    - You can use any of the email above and it should be same, please avoid sending large attachments
    - If you have large attachments, please use a cloud storage service
    - Keep in mind that email sent is public for everyone in the class to see

# Where is the PGP key located?
    - OpenPGP keys direct download: https://keys.openpgp.org/vks/v1/by-fingerprint/B72B50982DEDF2E0466F3DF5B7C1A2D2403189F6
    - Fingerprint: B72B 5098 2DED F2E0 466F 3DF5 B7C1 A2D2 4031 89F6
    - Alternatively, you can go to https://keyoxide.org/B72B50982DEDF2E0466F3DF5B7C1A2D2403189F6
    - Or, you can get the key from hkps://keys.openpgp.org using the OpenPGP client of your choosing by quering our fingerprint
    - Be very careful to double check our public key against the fingerprint
    - Spaces don't matter but make sure that the fingerprint in the key is the same as above.

# Why dont you sign everything?
    - This is because we dont see it necessary to sign every file here,
    we reasonably trust the cloud service providers we use.
    We only tend to protect link to the courses because that is at high risk of being altered.

# Is it not possible to mimic the key? How can I be sure the key that I get is the key from you?
    - It is possible to create a new key with the same name and email address as this key. However it will not be possible to
      mimic the fingerprint in the key. That is why we ask that you verify the fingerprint of the key before using it.


-----BEGIN PGP SIGNATURE-----

iQGzBAEBCAAdFiEEtytQmC3t8uBGbz31t8Gi0kAxifYFAmUxP6EACgkQt8Gi0kAx
ifYJYAv/VTBQs5uRFc4c+Jx9ZLFuwQ9dYHg+hHjj1tsfDh/XIcfXDxU39v1OZKDx
KfzL+VmAZ+iznW5dqT/Jk7Q2os0qIvd66AmNovCtEtfm1kKFgHjWCxVBPDm2gnaA
fqeyKc5gldh54V5SFdoWpubBnZZx0sJpY+a18pRMzzcUslwiMYifAIgZTydemg1l
LV/udN8QRLdhwqJgWHsfwBloWQ5Il4WHzHd5hI36Xqcv0fSQk7p4iwBzMgKywmY0
aqkyoJ0Ynb4eux/nKRq5NmMwpIrRdYWs2qKNkHooBEwzbanIuXx1MoU6WPWMYW1x
1MTZN3rGUTmEmHVB08aDAM4/CmQKPX8cGwgfNiAdH2B7aPNNgn9tDBJPt/H5rUzn
kCGABM58fkzbp9OHwb7+P54RZvIbE5SF9OuE6BzfM5rpz1mtwHUCZJ3hhQTeJtzp
ONdc012m2iUE4vsaNNP1UsdpGDw5U1LjewrLT6BR2dyUVV83wIsIPCq1DQcEA3T9
Nd1CRXno
=SI78
-----END PGP SIGNATURE-----
