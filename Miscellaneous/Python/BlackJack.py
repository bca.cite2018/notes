logo = """
__________ .__                    __             ____.                 __     
\______   \|  |  _____     ____  |  | __        |    |_____     ____  |  | __ 
 |    |  _/|  |  \__  \  _/ ___\ |  |/ /        |    |\__  \  _/ ___\ |  |/ / 
 |    |   \|  |__ / __ \_\  \___ |    <     /\__|    | / __ \_\  \___ |    <  
 |______  /|____/(____  / \___  >|__|_ \    \________|(____  / \___  >|__|_ \ 
        \/            \/      \/      \/                   \/      \/      \/ 
                                                                              
"""


import random
import os
blackjack_list = [10,'J','Q','K'] #Later we can see if the two chosen card forms a blackjack or not
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] #This is used to later check whether a given card is a face card or number card
cards = [
    {'A': [1,11]}, 
    {1: 1}, 
    {2: 2}, 
    {3: 3}, 
    {4: 4},
    {5: 5},
    {6: 6}, 
    {7: 7}, 
    {8: 8}, 
    {9: 9}, 
    {10: 10},
    {'J': 10},
    {'Q': 10},
    {'K': 10},
] #This is a list containing all the dictionaries that contain each of the card in key value pair
# I have used a dictionary inside a list because random.choice() only works well with list items and not dictionary items

def line(): #Simple function that puts a line in the console
    print('-------------------------------------')

def clear(): #Function to clear the terminal
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def sum_total(count_cards): #Function that calculates the sum of each item in the list and returns it
    sum = 0
    for card in count_cards:
        sum += card
    return sum

player_show_cards = [] #List for keeping track of the cards in hand of players that is shown to the player
player_count_cards = [] #List for keeping track of cards in hand of players that we can work out the total of
computer_count_cards = []
computer_show_cards = []

def display_player_hand(player_show_cards,player_count_cards,first = False): #Function to reduce the amount of typing to display user hand
    if first: #First is a variable that is passed as 'True' to indicate it is the very first card drawn in the start of the game
        print("\n")
        print(f"Your first card is {player_show_cards} which is of value {sum_total(player_count_cards)}")
        print("\n")
    else:
        print("\n")
        print(f"Your hand is {player_show_cards} totalling {sum_total(player_count_cards)}")
        print("\n")

def display_computer_hand(computer_show_cards,computer_count_cards,first = True): #Same function to reduce amount of typing to display computer hand
    if first: #In blackjack, the player can only see the first chosen card by the dealer, so sending first as True will only display the first chosen card
        print("\n")
        print(f"Computer's first card is {computer_show_cards[0]}, which is of value {sum_total(computer_count_cards)}.")
        print("\n")
    else:
        print("\n")
        print(f"Computer's hand is {computer_show_cards} totaling {sum_total(computer_count_cards)}.")
        print("\n")

def card_chooser(count_cards,show_card):
    return_card = {} #This is the dictionary that will be updated with chosen card information for output use of this function
    chosen_card = random.choice(cards)
    chosen_card = list(chosen_card.keys())[0] #Chosen card is a dictionary that only has one key and one value, so we just pull out the first key, which is the only key, we put it back in chosen_card
    if chosen_card not in numbers: #If the chosen card is not in numbers, ie. not a number card
        if chosen_card == 'A': #If chosen card is an 'A', then the value is 1 or 11 depending on the total cards in hand
            total_hand = sum_total(count_cards) #We calculate sum total to decide whether we need to put 'A' as 1 or 11.
            ace_list = cards[0]['A'] # Cards[0] is {'A':[1,11]} and referencing the key ['A'] only leaves us with the list [1,11] in ace_list
            if total_hand < 11:
                return_card['A'] = ace_list[1] #If total hand amount is less then 11, we put 'A' and 11 which is in the ace_list list at index 1
            else:
                return_card['A'] = ace_list[0]
        else:
            return_card[chosen_card] = 10 #If a card is a face card and not an 'A', we just put in the value as 10
    else:
        return_card[chosen_card] = chosen_card #If a card is a number card, just put in that number as the value

    count_cards.append(return_card[chosen_card]) #Add the value of return_card to the count card list
    show_card.append(list(return_card.keys())[0]) #Add the key of return_card to the show card list
    #It was important to make sure same card was added to count list and show card list. Hence we directly append to respective list

    return return_card #Return the actual card chosen, was useful in development and not so useful in game
           

def play_game(): #Function that starts the actual game
    game_is_on = True #game_is_on is used to keep track on whether the game is still playing or over
    card_chooser(player_count_cards,player_show_cards) #Choose a random card for the player
    card_chooser(computer_count_cards,computer_show_cards) #Choose a random card for the computer
    display_computer_hand(computer_count_cards,computer_count_cards) #Display only the first card in computer hand
    display_player_hand(player_show_cards,player_count_cards,True) #Display all the cards in player hand
    
    while game_is_on:
        exiter = 0 #This variable is used in just below while loop where we run the loop until len(computer_count_cards) < len(player_count_cards)
        next_or_pass = input("Enter 'next' for next card or 'pass' to pass: ").lower()
        while len(computer_count_cards) < len(player_count_cards): #Keep choosing cards in computer hand untill there are same number of chosen cards by player and computer
            card_chooser(computer_count_cards,computer_show_cards)

            first_two_cards_computer = [] #Here, we are checking if the first two cards in computer hand make up blackjack
            for i in range(2):
                    first_two_cards_computer.append(player_show_cards[i])
            if 'A' in first_two_cards_computer:
                    for possibility in blackjack_list:
                        if possibility in first_two_cards_computer:
                            line()
                            print(f"Computer got a black jack, your hand is {player_show_cards} so you win...")
                            print("\n")
                            exiter += 1 #exiter variable's purpose is to break out of the game_is_on while loop, because break only breaks out of the immediate while loop, I needed to find a different strategy
                            break
            
            if sum_total(computer_count_cards) == 21:
                line()
                print(f"Computer's hand is {computer_show_cards} which is 21 so computer wins")
                print("\n")
                game_is_on = False
                exiter += 1
                break

            if sum_total(computer_count_cards) > 21:
                line()
                print(f"Computer hand is {computer_show_cards} totalling {sum_total(computer_count_cards)} which exceeds 21 so you win")
                print("\n")
                game_is_on = False
                exiter += 1
                break

        if exiter == 0: #This simple hack again, dont run this below code if any of the above condition triggered exiter to go above 0
            if next_or_pass == 'next':
                card_chooser(player_count_cards,player_show_cards)
                display_player_hand(player_show_cards,player_count_cards)
                
                
                first_two_cards_player = [] #Checking if first two cards is a blackjack in player's hand
                for i in range(2):
                    first_two_cards_player.append(player_show_cards[i])

                if 'A' in first_two_cards_player: #Same thing about blackjack checking with the players hand
                    for possibility in blackjack_list:
                        if possibility in first_two_cards_player:
                            line()
                            print(f"You got a black jack, your hand is {player_show_cards} so you win...")
                            print("\n")
                            break
                
                if sum_total(player_count_cards) == 21:
                    line()
                    print(f"Your hand is {player_show_cards} which is exactly 21, you win")
                    print("\n")
                    break

                if sum_total(player_count_cards) > 21:
                    line()
                    print(f"You lose because your hand is {player_show_cards} totalling {sum_total(player_count_cards)} that exceeds 21")
                    print("\n")
                    break
                
            
            else: #This else is trigged when user chooses to pass.
                display_computer_hand(computer_show_cards,computer_count_cards,False) #By passing False as the last parameter, the function will show entire computer hand
                display_player_hand(player_show_cards,player_count_cards)
                if sum_total(computer_count_cards) > sum_total(player_count_cards): #If the user passes and noone crosses over 21, the person with higher amount of card in hand wins
                    print("\n")
                    line()
                    print(f"Computer hand is {computer_show_cards} totalling {sum_total(computer_count_cards)} which is higher than your total {sum_total(player_count_cards)} so you lose")
                    print("\n")
                elif sum_total(computer_count_cards) < sum_total(player_count_cards):
                    print("\n")
                    line()
                    print(f"Computer hand is {computer_show_cards} totalling {sum_total(computer_count_cards)} which is lower than your total {sum_total(player_count_cards)} so you win")
                    print("\n")
                else:
                    line()
                    print(f"Computer hand is {computer_show_cards} totalling {sum_total(computer_count_cards)} which is equal to your total {sum_total(player_count_cards)} so its a draw")
                    print("\n")
                game_is_on = False
            
                

      
play = True
while play:
    print("\n")
    to_play = input("Do you want to play BlackJack? Type 'yes': ").lower()
    if to_play == 'yes':
        player_show_cards = []
        player_count_cards = []
        computer_count_cards = []
        computer_show_cards = []
        clear()
        print(logo)
        play_game()
    else:
        print("Goodbye...")
        play = False
        exit(0)
        
        
        