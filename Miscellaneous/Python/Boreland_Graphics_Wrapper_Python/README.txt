This is a simple way to implement some functions from Boreland's Turbo C++ IDE in python.

INSTALLATION: All you have to do is Copy graph.py and boreland_wrapper.py in a place where python can see it.
              That is most likely where you are writing your new program

Credit:       Most of the credit goes to John Zelle, author of the book
              “Python Programming: An Introduction to Computer Science”
              I only made boreland_wrapper part

Licence:      I am not sure why I have to tell this but its the same as graph.py

After copying both graph.py and boreland_wrapper.py in the folder that you intend to write your own code, you can then import boreland_wrapper.py such as,

from boreland_wrapper import *

You just have to import boreland_wrapper and boreland_wrapper will import graph.py(the other file you downloaded) automatically

To create a window, call the initgraph function passing it window title, window width, and height.
It will then return a window object, you can use that as a window object to pass on to other functions for drawing,

for example,
win = initgraph("Hello",800,600)

Here, win is the new window object.

Most of the functions have parameter such as also_print which is initially set to False
If you pass in True, it will print the status in the terminal

Line function has a special parameter called arrowhead. It puts an arrow head in the line and accepts 'none','first','last','both', default is 'none'

Have a look at Example_code.py. Its just an example code, you dont need it.

Be sure to have a look at http://anh.cs.luc.edu/handsonPythonTutorial/graphics.html#the-documentation-for-graphics-py and https://mcsp.wartburg.edu/zelle/python/graphics/graphics/graphref.html