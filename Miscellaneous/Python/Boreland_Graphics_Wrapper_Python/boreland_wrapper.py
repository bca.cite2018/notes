from graph import *
def initgraph(title,width,height):
    """Creates a window and returns that window"""
    window = GraphWin(title,width,height)
    print("Graphics window called")
    return window
    
def closegraph(window_object,also_print = False):
    """Closes the window provided the window object"""
    if also_print == True:
        print(f"Graphics window {window_object} closed")
    window_object.close()
    

def putpixel(x,y,window_object,also_print = False,color='black'): #Function to place a pixel on the screen given x and y coordinates
    """Puts a point in the specified location"""
    if also_print == True:
        print(f"Pixel was put in the position ({x},{y})")
    pt = Point(x,y)
    pt.setFill(color)
    pt.draw(window_object)

def outtextxy(x,y,text,window_object,font_color='black',font_size=10,also_print = False): #Function to place a text on the screen given x, y coordinates and text
    """Writes a text in a specified position in the screen"""
    if also_print == True:
        print(f"Text output: {text} on position ({x},{y}) of color={font_color}")
    message = Text(Point(x,y),text)
    message.setFill(font_color)
    message.setSize(font_size)
    message.draw(window_object)

def circle(center_x,center_y,radius,window_object,outline_color = 'black', fill_color = 'white',also_print = False):
    """Draws a circle given the center coordinate and the radius"""
    if also_print == True:
        print(f"Circle centered at ({center_x},{center_y}) drawn with radius {radius} of with border color={outline_color} and fill color={fill_color}")
    cir = Circle(Point(center_x,center_y),radius)
    cir.setOutline(outline_color)
    cir.setFill(fill_color)
    cir.draw(window_object)

def bar(top_left_x,top_left_y,bottom_right_x,bottom_right_y,window_object,outline_color='black',fill_color='white',also_print=False):
    """Draws a rectangle with opposite diagonals"""
    if also_print == True:
        print(f"Bar drawn from ({top_left_x},{top_left_y}) to ({bottom_right_x},{bottom_right_y}) with brder color={outline_color} and fill color={fill_color}")
    rect = Rectangle(Point(top_left_x,top_left_y), Point(bottom_right_x,bottom_right_y))
    rect.setOutline(outline_color)
    rect.setFill(fill_color)
    rect.draw(window_object)

def line(x1,y1,x2,y2,window_object,color='black',also_print = False,arrowhead='none'):
    """Draws a line with given two points"""
    if also_print == True:
        print(f"Line drawn from ({x1},{y1}) to ({x2},{y2}) with arrowhead attached at {arrowhead}")
    l = Line(Point(x1,y1),Point(x2,y2))
    l.setFill(color)
    l.setArrow(arrowhead)
    l.draw(window_object)




