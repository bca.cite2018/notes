#If graphics is not found, run the command: pip install graphics.py
#from graphie import *
from boreland_wrapper import *
#import time
#import os
logo = """

 / \---------------------, 
 \_,|                    | 
    |    DDA Algorithm   | 
    |  ,-------------------
    \_/__________________/
    
"""

def pause():
    """Runs a pause command in your shell"""
    if os.name == 'nt':
        os.system('pause')
    elif os.name == 'posix':
        os.system('read -n1 -r -p "Press any key to continue..." key')
    else:
        print("Will pause for 5 seconds...")
        time.sleep(5)

def convert_into_positive(num):
    """Converts a number into positive if it is negative"""
    if num < 0:
        num = num * -1
        return num
    else:
        return num
    
print(logo)
initial_pos = [] #x1,y1
final_pos = [] #x2,y2

initial_pos.append(int(input("Enter x1: ")))
initial_pos.append(int(input("Enter y1: ")))
print("\n")
final_pos.append(int(input("Enter x2: ")))
final_pos.append(int(input("Enter y2: ")))

difference_in_x = final_pos[0] - initial_pos[0] #x2 - x1
difference_in_y = final_pos[1] - initial_pos[1] #y2 - y1

if convert_into_positive(difference_in_x) > convert_into_positive(difference_in_y):
    steps = convert_into_positive(difference_in_x)
else:
    steps = convert_into_positive(difference_in_y)

x_increment = difference_in_x / steps
y_increment = difference_in_y / steps
new_x = initial_pos[0] #Right now, they are initial_position but they will be updated in the below for loop
new_y = initial_pos[1]

window = initgraph("DDA Line drawing Algorithm",800,600)

for drawing in range(steps):
    time.sleep(0.5) # Wait 0.5 seconds for seeing the lines drawn slowly
    new_x = new_x + x_increment
    new_y = new_y + y_increment
    putpixel(round(new_x), round(new_y),window,True,'red')
print("\n")
outtextxy(200,20, "DDA",window); # for printing text around desired screen location
outtextxy(initial_pos[0] + 5, initial_pos[1] - 5, "(x1,y1)",window)
outtextxy(final_pos[0] + 5, final_pos[1] + 5, "(x2,y2)",window,True)
pause()
closegraph(window)
exit(0)