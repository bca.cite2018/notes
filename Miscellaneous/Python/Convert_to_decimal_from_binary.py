process_array = []
answer_array = []
answer = 0
number = input("Enter a number: ")
for indexx in number:
    if indexx in ['0','1']:
        process_array.append(int(indexx))
    else:
        print("Invalid Input, must be 1 or 0 only")
        exit()

process_array_length = len(process_array) - 1
for individual in process_array:
    answer_array.append(individual * 2 ** process_array_length)
    process_array_length -= 1

for elements in answer_array:
    answer+= elements

print(f"Converted to decimal: {answer}")