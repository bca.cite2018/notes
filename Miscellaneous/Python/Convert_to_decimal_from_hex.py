hex_characters = {'A': 10, 'B': 11, 'C': 12, 'D': 13, 'E': 14, 'F': 15}
question = input("Enter the hex number: ").upper()
question_array = []
process_array = []
answer = 0

for digits in question:
    if (ord(digits) > 47 and ord(digits) < 58) or (ord(digits) > 64 and ord(digits) < (65 + 6)):

        if digits in hex_characters:
            question_array.append(int(hex_characters[digits]))
        else:
            question_array.append(int(digits))

    else:
        print("Invalid Input")
        exit(1)

question_length = len(question_array) - 1

iterator = 0
for digits in range(question_length, -1, -1):
    process_array.append(question_array[digits] * 16 ** iterator)
    iterator += 1

for individual in process_array:
    answer += individual

print(f"The final answer is {answer}")
