#!/bin/python

# Credit for this actually goes to Dr. Angela and the Appbrewery team in their Udemy course "100 days of python code"
# I just made a few more improvements

logo = """           
 ,adPPYba, ,adPPYYba,  ,adPPYba, ,adPPYba, ,adPPYYba, 8b,dPPYba,  
a8"     "" ""     `Y8 a8P_____88 I8[    "" ""     `Y8 88P'   "Y8  
8b         ,adPPPPP88 8PP"""""""  `"Y8ba,  ,adPPPPP88 88          
"8a,   ,aa 88,    ,88 "8b,   ,aa aa    ]8I 88,    ,88 88          
 `"Ybbd8"' `"8bbdP"Y8  `"Ybbd8"' `"YbbdP"' `"8bbdP"Y8 88   
            88             88                                 
           ""             88                                 
                          88                                 
 ,adPPYba, 88 8b,dPPYba,  88,dPPYba,   ,adPPYba, 8b,dPPYba,  
a8"     "" 88 88P'    "8a 88P'    "8a a8P_____88 88P'   "Y8  
8b         88 88       d8 88       88 8PP""""""" 88          
"8a,   ,aa 88 88b,   ,a8" 88       88 "8b,   ,aa 88          
 `"Ybbd8"' 88 88`YbbdP"'  88       88  `"Ybbd8"' 88          
              88                                             
              88           
"""

def clear_terminal(): #function to clear the terminal
    from os import system,name
    if name == 'nt': #If it is running on Windows cmd
        system('cls') #Run command cls
    else:
        system('clear') #If it is not windows cmd, run command clear
        # Note that os.name would have been 'posix' for Mac or GNU/Linux running bash
        # but other non bash shells also accept clear as a command to clear the screen
alphabet = [' ', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}','~']

#The block of code below is not necessary and is therefore commented. Its here just as a note on how the above alphabet list can be made using the ASCII table
#----------------------------------------------------
#alphabet = [] #Initialise alphabet with an empty list

#for ascii_value in range(32, 126 + 1): #Run a for loop from 32 to 126 inclusive
#    alphabet += chr(ascii_value) #Put everything from ASCII character ' ' to '~' in alphabet
#----------------------------------------------------
#Make sure you comment line 30, ie. line with alphabet list if you uncomment the commented block above

def caesar(start_text, shift_amount, cipher_direction):
  end_text = "" #Place for final text
  if cipher_direction == "decrypt":
    shift_amount *= -1 #If the user had asked to decrypt, we can just negate shift_amount so that we can just add to substract
  for char in start_text: #Loop through start_text and place it in 'char' in each step
    if char in alphabet:  #If 'char' exists in alphabet list
        position = alphabet.index(char) #First we find out the index of user input character in alphabet list
        new_position = position + shift_amount #We calculate the new position by adding the shift amount to the index of character from 'char' variable
        if new_position >= len(alphabet) or new_position < 0: #If shift amount is larger than the length of alphabet, we just cycle around alphabet
            new_position %= len(alphabet)
        end_text += alphabet[new_position]
    else: #If user enters weird unicode characters, we will not shift it
        end_text += char
        
  print(f"\nHere's the {cipher_direction}ed result: \n\n{end_text}") #Display the end text
  print("----------------------------------------------------")
  # the variable cipher_direction will be either "encrypt" or "decrypt" so we can cleverely display
  # {cipher_direction} which will be {encrypt}ed or {decrypt}ed which will be correctly shown by the f-string
  
yes = True #This variable will be changed to False once the user wants to exit the program
while yes:
    clear_terminal() #Comment this line if you dont want to clear the terminal after each use
    print(logo)
    direction = input("Type 'encrypt' to encrypt, type 'decrypt' to decrypt [Anything else means encrypt]:\n")
    text = input("\nType your one line message and press Enter [Note that non standard unicode characters wont be shifted]: \n")
    shift = round(float(input("\nType the shift number [Should be same for encryption and decryption, numbers with decimal point gets rounded]:\n")))
    caesar(start_text=text, shift_amount=shift, cipher_direction=direction) #Passing values over to caesar function
    redo = input("Do you want to do it again? Type yes or no. Anything else is treated as yes\n").lower()
    if redo == "no":
        print("\n\nGoodbye")
        yes = False #When user wants to exit the program, we change yes to False so that the while loop wont run again
        exit(0) #Exit with error code 0 on program exit
