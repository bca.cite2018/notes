import random

def get_line():
    return "------------------------------------------"

def symbol_list(): #This function collects all the symbols from ASCII value 32 to 126 excluding numbers and alphabets
    symbols = ['']*((64-32)+(96-91)+(126-123) -7)
    iterator = 0
    for symbol in range(32,127):
        if symbol > 64 and symbol < 91:
            symbol += 1
            continue
        
        if symbol > 96 and symbol < 123:
            symbol += 1
            continue
        
        if symbol > 47 and symbol < 58:
            symbol += 1
            continue
        
        
        
        symbols[iterator] = chr(symbol)
        iterator += 1
        
        
    return symbols

def number_list(): #This function collects all the numbers from ASCII table
    numbers=['']*10
    for number in range(0,10):
        numbers[number]= chr(number + 48)
        
    return numbers

def alphabet_list(): #This function collects all the alphabets including both uppercase and lowercase
    alphabets = ['']*(26*2)
    for uppercase_alphabet in range(0,26,1):
        alphabets[uppercase_alphabet] = chr(uppercase_alphabet+65)
        
    
    for lowercase_alphabet in range(26,52):
        alphabets[lowercase_alphabet] = chr((lowercase_alphabet-26)+97)
        
    return alphabets

def generator(numbers,alphabets,symbols):
    numbers -= 1
    alphabets -= 1
    symbols -= 1
    total_generated = ['']
    alphabet_iterator = 0
    symbol_iterator = 0
    number_iterator = 0
    while alphabets >= 0:
        total_generated.insert(alphabet_iterator,alphabet_list()[random.randint(0,alphabets)])
        alphabets-= 1
        alphabet_iterator += 1
        
    while numbers >= 0:
        total_generated.insert(number_iterator,number_list()[random.randint(0,numbers)])
        numbers -= 1
        number_iterator += 1
        
        while symbols >= 0:
            total_generated.insert(symbol_iterator,symbol_list()[random.randint(0,symbols)])
            symbols -= 1
            symbol_iterator += 1
    random.shuffle(total_generated)
    
    return ''.join(total_generated)

def welcome_text():
    print("""Welcome to a simple password manager written in Python
DISCLAIMER: THERE IS NO ENCRYPTION OR ANYTHING, USE WITH CAUTION
    """)
        
        
def menu():
    welcome_text()
    final_list = str('')
    database_name = input("Enter the name of your password file to open: ")
    if database_name == '':
        print("You did not enter any filename therefore your passwords wont be saved.")
    database = open(database_name,'a')
    database_temp = open(database_name, 'r')
    print("Already saved passwords in the database:")
    print(database_temp.read())
    print(get_line())
    truth = True
    while truth:
        username = input("Enter a new username for this entry: ")
        website = input("Enter the website so that you remember where to use this password: ")
        comment = input("Enter a one line comment: ")
        choice = input("If you want your password generated, enter 'g':")
        if choice == 'g':
            how_many_numbers=(int(input(("Enter how many numbers do you want? "))))
            how_many_alphabets=(int(input("Enter how many symbols do you want? ")))
            how_many_symbols = (int(input("Enter how many alphabets do you want? ")))
            password =generator(how_many_numbers,how_many_alphabets,how_many_symbols)
        else:
            password = input("Enter the password: ")
        new_list = (f"\n------------------------------------------\nusername: {username}\nwebsite: {website}\npassword: {password}\ncomment: {comment}")
        final_list = final_list + new_list
        print(final_list)
        print(get_line())
        database.write(new_list)
        loop_choice = input("If you want to add another entry, press 'a': ")
        if loop_choice != 'a':
            truth = False
        else:
            print(get_line())


menu()