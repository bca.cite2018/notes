hex_characters = ['A', 'B', 'C', 'D', 'E', 'F']
decimal = int(input("Enter a decimal number: "))
hex = []

while True:
    moded_hex = decimal % 16 #At each step, divide by 16 for its remainder
    if moded_hex >= 10: #If the answer is greater than or equal to 10, push hex characters instead
        hex.append(hex_characters[moded_hex % 10])
    else:
        hex.append(str(moded_hex)) #Else push the number itself
    decimal = decimal // 16 #Divide the decimal by 16 for the next step
    if decimal <= 0:
        break

hex.reverse()
print(''.join(hex))
