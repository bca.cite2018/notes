## What are these
These are font files that you can install in your computer

## Why are they here
I realised that it is very difficult to find these font when you are not using Microsoft Windows. You need the Times New Roman font for reports

## How can I install it
### Linux
* Create a new directory in your home directory named ``.fonts``
* Open that folder (Note that since it has a . at front, it will be hidden, either show all files in your file manager or use the terminal to do this)
* Copy all the ``.ttf`` files directly inside ``.fonts``
* Restart whatever text processor or editor you are using and you should see them in the list of fonts. This is confirmed working with Libreoffice suite