#include <iostream>
bool is_prime(int num)
{
    int flag = 1; // Flag starts out as 1 because any number * 1 is that same number. Will make sense later
    if(num < 0) // We want to see if the number is negative
    {
        num *= -1; // Negative numbers should be converted into positive by multiplying -1
        flag = -1; // We will change the flag variable to -1 so that the change can be reversed later when displaying
    }
    if(num <= 3) // We assume 1 as prime even thoug it is both prime and not prime
    {
        return true;
    }
    else if(num % 2 == 0) // This elseif statement will only be checked after the above if. Which means we can be sure the number is greater than 3
    {
        std::cout<<2 * flag<<" * "<<(num / 2)<<" = "<<num<<std::endl;
        return false;
    }
    else
    {
        int halved = (num / 2) + 1; // What is the last number that divides 50? Its 25. Which means no other number greater than 25 will ever divide 50. This is an example to sorta explain whats going on. We add 1 for absolutely safety though it may not be necessary always
        for(int i = 3; i <= halved; i += 2) // The pattern of odd and even number goes as odd, even, odd, even... Meaning if we add 2 to an odd number, the reuslt is also an odd number
        {
            if(num % i == 0) // Whats prime number anyways? If a number is disivible by any other number other than 1 and itself, it is prime, so we need to check that
            {
                std::cout<<i * flag<<" * "<<(num / i)<<" = "<<num<<std::endl;
                return false; // As soon as a return statement is found, the funcion exits, meaning next line will not be run
            }
            return true; // If the loop finishes and found no divisor, the function should return false
        }
    }
}

int main()
{
    std::string out;
    int input_num;
    std::cout<<"Enter a number: "<<std::endl;
    std::cin>>input_num;
    if(is_prime(input_num))
    {
        out = "The provided number is prime";
    }
    else
    {
        out = "The provided number is not prime";
    }
    std::cout<<out<<std::endl;
    return 0;
}