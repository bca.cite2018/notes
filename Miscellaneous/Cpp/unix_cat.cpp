#include<iostream>
#include<fstream>
#include<string>
int main(int argc, char** argv)
{
    if(argc == 1)
    {
        std::cerr<<"ERROR: Expected atleast one text file"<<std::endl;
        std::cerr<<"Syntax: cat [FILE1] [FILE2] ... "<<std::endl;
        return -400;
    }
    std::string line;
    std::ifstream text;
    for(int i = 1; i < argc ; i++)
    {
        text.open(argv[i], std::ios_base::in);
        if(!text.is_open())
        {
            std::cerr<<"ERROR: "<<argv[i]<<" Does not exist, did you forget to include the file extension?"<<std::endl;
            return -404; // Because why not?
        }
        while(getline(text, line))
        {
           std::cout<<line<<std::endl;
        }
        text.close();
    }
    return 0;
}
